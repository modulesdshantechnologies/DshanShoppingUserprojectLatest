var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    productDetailsModel = mongoose.model('Product'),
     Categories = mongoose.model('Categories'),
        subCategoriesDetailsModel = mongoose.model('SubCategories');

    module.exports = function (app){
        app.use('/', router);
    };

router.post('/product', function(req, res, next) {

    var newproductDetailsModel = new productDetailsModel(req.body);
    productDetailsModel.count(function(err,productCount){
    if(productCount!=0)
    {

     console.log(productCount);
     newproductDetailsModel.productId=productCount+1;

    newproductDetailsModel.save(function(err,result) {
        if (err){
            console.log('Error in Saving user: '+err);
        }
        console.log(result);
        res.send(result);
    });
    }
    else if(productCount==0)
    {
     console.log(productCount);
         newproductDetailsModel.productId=productCount+1;

        newproductDetailsModel.save(function(err,result) {
            if (err){
                console.log('Error in Saving user: '+err);
            }
            res.send(result);
        });
    }
});
});



router.get('/allProduct', function(req, res, next) {
 productDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.post('/editproductBymongoId', function(req, res, next) {
console.log(req.body);
console.log(req.body._id);
 productDetailsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.delete('/productBymongoId/:productMongoid',function(req, res, next){
productDetailsModel.remove({"_id":req.params.productMongoid},function(err,result)
{
if(err)
{
 console.log(err.stack)
}
else
{
 res.send(result)
}

});
});


//router.get('/productName',function(req,res,next){
//
//
//productDetailsModel.findOne(function(err,result){
//                    if(err)
//                        {
//                         console.log(err.stack)
//                        }
//                     else
//                      {
//                         console.log(result);
//                         res.send(result);
//
//                        }
//
//
//                       })
//
//})

router.post('/productByName/:productName',function(req,res,next){

console.log(req.params.productName);
productDetailsModel.find({"productName":req.params.productName},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})

router.get('/productByMongoId/:productMongoId',function(req,res,next){

productDetailsModel.find({"_id":req.params.productMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err.stack)
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

})




router.route('/productListByCategory')
    .get(function(req,res){
        Categories.aggregate([
            {$lookup:
                {
                    from:"Product",
                    localField: "categoryId",
                    foreignField:"categoryId",
                    as: "ProductUnderCategory"
                }
            }
        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                    res.send(result)
                }

            })


    });



router.route('/productListBySubCategory')
    .get(function(req,res){
        SubCategories.aggregate([
            {$lookup:
                {
                    from:"Product",
                    localField:"subCategoryid",
                    foreignField:"subCategoryid",
                    as: "SubCategories"
                }
            }
        ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                console.log(result)
                    res.send(result)
                }

            })


    });




router.route('/productListByCategoryAndSubCategory')
    .get(function(req,res){
        Categories.aggregate([
            {$lookup:
                {
                    from:"SubCategories",
                    localField:"categoryId",
                    foreignField:"categoryId",
                    as: "SubCategoriesbyCategory"
                }
            }
        ,{
              $unwind:"$SubCategoriesbyCategory"
    },{
              $lookup: {
                  from:"Product",
                  localField: "subCategoryid",
                  foreignField:"subCategoryid",
                  as:"productList"
              }
              },{
                  $unwind:"$productList"
                }
               ],function (err, result) {
                if (err) {
                    res.send(err)
                }
                else{
                console.log(result)
                    res.send(result)
                }

            })


    });


