// app.js
//angulasJS code for URL ROUTING

var ShoppingCardUser = angular.module('eCommerce', ['ui.router']);

//ui routing
ShoppingCardUser.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/pages/home');

    $stateProvider

        .state('home', {
            url: '/pages/home',
            templateUrl: 'pages/home.html'
        })
.state('template', {
            url: '/template',
            templateUrl: 'views/template.html'
        })


        .state('template2', {
                    url: '/template2',
                    templateUrl: 'views/template2.html'
                })

        .state('market', {
            url: '/supermarket/market',
                    templateUrl: 'supermarket/market.html'
                })

        .state('about', {
            url: '/pages/about',
            templateUrl: 'pages/about.html'
        })
        .state('products', {
            url: '/products/product',
            templateUrl: 'product.html'
        })

        .state('mail', {
            url: '/pages/mail',
            templateUrl: 'pages/mail.html'
        });
});

//http request
ShoppingCardUser.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
});
//setting URL constant to point to common directory
ShoppingCardUser.constant('URL', '../data/');

//service to get data from content.json
ShoppingCardUser.factory('DataService', function($http, URL) {
    var getData = function() {
    return $http.get(URL + 'content.json');
    };
    return{
        getData : getData
        };
});

//service to get Template from templates.json
ShoppingCardUser.factory('TemplateService', function($http, URL) {
    var getTemplates = function() {
    return $http.get(URL + 'templates.json');
    };
    return{
        getTemplates : getTemplates
        };
});

//controller to render data

ShoppingCardUser.controller('myController', function (DataService) {
    var ctrl = this;

     ctrl.content = [];

     ctrl.fetchContent = function () {
        DataService.getData().then(function (result) {
          ctrl.content = result.data;
        });
      };
      ctrl.fetchContent();
});


//Custom directive   Passw0rDO0
ShoppingCardUser.directive('contentItem',function ($compile, TemplateService) {
    var getTemplate = function (templates, contentType) {
        var template = '';

        switch (contentType) {
            case 'newstitle' :
                template = templates.newsTemplate;
                break;

            case 'footer' :
                             template = templates.footerTemplate;
                             break;

         /*   case 'information' :
                            template = templates.infoFooterTemplate;
                            break;

            case 'category' :
                            template = templates.categoryFooterTemplate;
                            break;

            case 'profile' :
                            template = templates.profileFooterTemplate;
                            break;

            case 'copyrights' :
                            template = templates.copyrightsFooter;
                            break;
     */   }
            return template;

    };

    var linker = function (scope, element, attrs) {
        TemplateService.getTemplates().then(function (response) {
            var templates = response.data;
            element.html(getTemplate(templates, scope.content.content_type));
            $compile(element.contents()) (scope);
         });
    };

    return {
        restrict: 'E',
        link: linker,
        scope: {
            content: '='
         }
    };
    });

